import json

import mediawiki_new_errors_checker as mwnec


OLD_DASHBOARD = {
    "attributes": {
        "description": "A view of 'mediawiki-errors' that excludes "
                       "known #wikimedia-prod-error tasks.",
        "hits": 0,
        "kibanaSavedObjectMeta": {
            "searchSourceJSON": (
                "{"
                "\"highlightAll\":true,"
                "\"version\":true,"
                "\"query\":{"
                "\"query\":\"\","
                "\"language\":\"lucene\""
                "},\"filter\":[{"
                "\"meta\":{"
                "\"alias\":\"TX1234 - Lock timeout\","
                "\"negate\":true,"
                "\"disabled\":false,"
                "\"type\":\"phrase\","
                "\"key\":\"exception.message\","
                "\"params\":{"
                "\"query\": \"Function: Upload\""
                "}"
                "}},{"
                "\"meta\":{"
                "\"alias\":\"T1234 - Lock timeout\","
                "\"negate\":true,"
                "\"disabled\":false,"
                "\"type\":\"phrase\","
                "\"key\":\"exception.message\","
                "\"params\":{"
                "\"query\": \"Function: Upload\""
                "}"
                "}},{"
                "\"meta\":{"
                "\"alias\":\"T5678 - Lock timeout\","
                "\"negate\":true,"
                "\"disabled\":false,"
                "\"type\":\"phrase\","
                "\"key\":\"exception.message\","
                "\"params\":{"
                "\"query\": \"Function: Upload\""
                "}"
                "}}"
                "]"
                "}"
            )
        },
        "title": "mediawiki-new-errors",
        "version": 1
    },
    "type": "dashboard",
    "everything_else": "should be the same"
}


def test_valid_tasks():
    valid_tasks = [
        'T123 - hi mom',
        'T123: hi mom',
        'T123 hi mom',
        'T123 – hi mom',
    ]
    invalid_tasks = [
        'Tyler is cool',
        'T 123',
    ]
    for valid_task in valid_tasks:
        assert mwnec.LogstashDashboard._get_task_id(valid_task) is not None
    for valid_task in invalid_tasks:
        assert mwnec.LogstashDashboard._get_task_id(valid_task) is None


def test_generate_new_dashboard():
    lsd = mwnec.LogstashDashboard(dashboard_id="666")
    new_dashboard = lsd._generate_new_dashboard(
        filter_bugs=['T1234'],
        old_dashboard=OLD_DASHBOARD
    )
    assert new_dashboard is not None

    new_filters = [f['meta']['alias'] for f in json.loads(
        new_dashboard['attributes']['kibanaSavedObjectMeta']['searchSourceJSON']
    )['filter']]

    # We should have left alone non task filters
    assert 'TX1234 - Lock timeout' in new_filters

    # We should have left alone non-matching task filters
    assert 'T5678 - Lock timeout' in new_filters

    # We should have removed things we tried to filter
    assert 'T1234 - Lock timeout' not in new_filters

    # And everything else should be the same
    assert new_dashboard['everything_else'] == 'should be the same'

    # And we shouldn't touch the old version of the dashboard
    old_filters = [f['meta']['alias'] for f in json.loads(
        OLD_DASHBOARD['attributes']['kibanaSavedObjectMeta']['searchSourceJSON']
    )['filter']]

    assert 'T1234 - Lock timeout' in old_filters
